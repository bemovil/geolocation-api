
from django.urls import path, include
from rest_framework import routers
from .views import Geolocation, GeolocationList
urlpatterns = [
    path('Geolocation/', Geolocation.as_view(), name='Geolocation'),
    path('GeolocationList/', GeolocationList.as_view(), name='GeolocationList'),

]
