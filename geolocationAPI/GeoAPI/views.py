
import json
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet, ModelViewSet
from rest_framework import status

from folium.plugins import FastMarkerCluster
import folium
from geopy.geocoders import Nominatim
import geopy
from geopy.extra.rate_limiter import RateLimiter
from rest_framework import generics
import pandas as pd
from io import StringIO
from rest_framework.response import Response
from geopy.exc import GeocoderTimedOut

import time
from .serializers import FileUploadSerializer

import requests

# Acquire from developer.here.com



class GeolocationList(APIView):

    def get(self, request, *args, **kwargs):
        start_time = time.time()


        df1 = pd.read_json('natural.json')
        df2 = pd.read_json('juridica.json')
        lst = df1['fields'].tolist()
        lst2 = df2['fields'].tolist()
        lst.extend(lst2)
        # print(lst)
        locator = Nominatim(user_agent='http')
        # print("Geolocation file is processing")
        # print('{} could not be geocoded'.format(i['address']))
        #             continue
        final = []
        for i in lst:
            if (i['address'] and i['status'] == True):
                x = i['address'] + ',' + i['city'] + ',' + 'Colombia'
                final.append(x)
                
            else:
                print('the status of this record is rejected')
                continue
                
                # print("final", final)
        LatLong = []
        for address in final:
            # time.sleep(1)
            # print(address)
            # location = input("Enter the location here: ") #taking user input
            URL = "https://geocode.search.hereapi.com/v1/geocode"
            # location = input("Enter the location here: ") #taking user input
            api_key = 'YWHhRDSh7-w-eRzrF1CYJVWdRcGIzK8wmeBqYtYaNbU' 
            PARAMS = {'apikey':api_key,'q':address} 
            # sending get request and saving the response as response object 
            r = requests.get(url = URL, params = PARAMS) 
            data = r.json()
            # print(data)
            if data['items']==[]:
                continue
            else:
                latitude = data['items'][0]['position']['lat']
                longitude = data['items'][0]['position']['lng']
                # print(latitude)
                # print(longitude)
                LatLong.append((latitude, longitude, address))

            # g = locator.geocode(address)

            # if g is None:
            #     print('{} could not be geocoded'.format(address))
            # else:
            #     print(g.address)
            #     print((g.latitude, g.longitude))
        print("--- %s seconds ---" % (time.time() - start_time))
    
        return Response({"data": LatLong},  status=status.HTTP_200_OK)


class Geolocation(generics.CreateAPIView):
    serializer_class = FileUploadSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = serializer.validated_data['file']
        decoded_file = file.read().decode('utf-8')
        # upload_products_csv.delay(decoded_file, request.user.pk)
        locator = Nominatim(user_agent='http')
        print("Geolocation file is processing")
        df = pd.read_csv(StringIO(decoded_file), delimiter=',')
        df['ADDRESS'] = df['Address1'].astype(
            str) + ',' + df['Address3'] + ',' + df['Address4'] + ',' + df['Address5'] + ',' + ' Sweden'
        geocode = RateLimiter(locator.geocode, min_delay_seconds=1/2)
        df['location'] = df['ADDRESS'].apply(geocode)
        df['point'] = df['location'].apply(
            lambda loc: tuple(loc.point) if loc else None)
        print(df['point'])
        # df['point'].dropna()
        df[['latitude', 'longitude', 'altitude']] = pd.DataFrame(
            df['point'].tolist(), index=df.index).dropna()
        # print(df.columns)
        df = df.drop(['Address1', 'Address3', 'Address4', 'Address5',
                      'Telefon', 'ADDRESS', 'location'], axis=1)
        df.latitude.isnull().sum()
        df = df[pd.notnull(df["latitude"])]
        # map1 = folium.Map(
        #     location=[59.338315, 18.089960],
        #     tiles='cartodbpositron',
        #     zoom_start=12,
        # )
        # df.apply(lambda row: folium.CircleMarker(
        #     location=[row["latitude"], row["longitude"]]).add_to(map1), axis=1)
        # print(map1)
        # x = {'map': list(map(lambda x: {'data': x}, map1))}
        # y = json.dumps(x)
        # io_string = io.StringIO(decoded_file)
        # reader = csv.reader(io_string)
        # for row in reader:
        #     print(row)
        return Response({"data": df['point']},  status=status.HTTP_200_OK)
